<#include "/part/header.ftl">

<#if errors??>
    <div class="alert alert-danger" role="alert">
        <#list errors as error>
            <p>${error}</p>
        </#list>

    </div>
</#if>

<form method="post" action="<#if book?? && book.id??>/book/update/${book.id}<#else>/book/add</#if>" name="book">
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" name="name" <#if book?? && book.name??>value="${book.name}"></#if>
    </div>
    <div class="form-group">
        <label >Author</label>
        <input type="text" class="form-control" name="author" <#if book?? && book.author??>value="${book.author}"></#if>
    </div>
    <input class="btn btn-primary" type="submit" value="Submit">
</form>

<#include "/part/footer.ftl">