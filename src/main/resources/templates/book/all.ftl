<#include "/part/header.ftl">
    <h1>All books</h1>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-striped">
                <tr id="columnHeader">
                    <th id="id_column">ID</th>
                    <th>Name</th>
                    <th>Author</th>
                    <th>Actions</th>
                </tr>
                <#list bookList as book>
                    <tr id="columnData">
                        <td>${book.id}</td>
                        <td>${book.name}</td>
                        <td>${book.author}</td>
                        <td>
                            <span class="glyphicon glyphicon-pencil"></span>
                            <a href="/book/update/${book.id}">Edit</a>
                            <span class="glyphicon glyphicon-remove"></span>
                            <a href="/book/delete/${book.id}">Delete</a>
                        </td>
                    </tr>
                </#list>
            </table>
        </div>
        </div>

<#include "/part/footer.ftl">
