<#list books as book>
    <tr>
        <td>${book.id}</td>
        <td>${book.name}</td>
        <td>${book.author}</td>
        <td>
            <span class="glyphicon glyphicon-pencil"></span>
            <a href="/book/update/${book.id}">Edit</a>
            <span class="glyphicon glyphicon-remove"></span>
            <a href="/book/delete/${book.id}">Delete</a>
        </td>
    </tr>
</#list>