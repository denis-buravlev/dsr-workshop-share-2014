package project.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import project.entity.Book;

import java.util.List;

@Repository
public interface BookRepository extends CrudRepository<Book, Integer> {

    @Query("SELECT b from Book b where b.id >= :value")
    List<Book> getBook(@Param("value") Integer value);

    @Query("SELECT b FROM Book b ORDER BY b.id DESC")
    List<Book> getInverse();
}
