package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import project.entity.Book;
import project.service.BookService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {

	@Autowired
    private BookService bookService;

    @RequestMapping("")
    public String getAll(ModelMap modelMap) {
        modelMap.addAttribute("bookList", bookService.getAll());
        return "book/all";
    }

	@RequestMapping("/{id}")
	public String getOne(@PathVariable Integer id, ModelMap modelMap) {
		modelMap.addAttribute("book", bookService.getOne(id));
		return "book/one";
	}

	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable Integer id) {
		bookService.delete(id);
		return "redirect:/book";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addForm() {
		return "book/addForm";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(@ModelAttribute("book") @Valid Book book,
                      BindingResult bindingResult,
                      ModelMap modelMap)  {
        if (bindingResult.hasErrors()) {
            List<String> validationErrors = new ArrayList<String>();
            for (FieldError error : bindingResult.getFieldErrors()) {
                validationErrors.add(error.getDefaultMessage());
            }
            modelMap.addAttribute("errors", validationErrors);
            return "book/addForm";
        }
		bookService.add(book);
		return "redirect:/book";
	}

    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public String updateForm(@PathVariable Integer id, ModelMap modelMap) {
        modelMap.addAttribute("book", bookService.getOne(id));
        return "book/addForm";
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    public String update(@PathVariable Integer id,
                         @ModelAttribute("book") @Valid Book book,
                         BindingResult bindingResult,
                         ModelMap modelMap) {
        if (bindingResult.hasErrors()) {
            List<String> validationErrors = new ArrayList<String>();
            for (FieldError error : bindingResult.getFieldErrors()) {
                validationErrors.add(error.getDefaultMessage());
            }
            modelMap.addAttribute("errors", validationErrors);
            return "book/addForm";
        }
        bookService.update(id, book);
        return "redirect:/book";
    }

    @RequestMapping(value = "/getInverse")
    public String getInverse(ModelMap modelMap) {
        modelMap.addAttribute("books", bookService.getInverse());
        return "book/inverse";
    }
}
