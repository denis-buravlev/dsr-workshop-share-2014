package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.entity.Book;
import project.repository.BookRepository;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
public class BookService {

    @Autowired
    BookRepository bookRepository;

    @Transactional
	public List<Book> getAll() {
        List<Book> books = new ArrayList<Book>();
        for(Book book : bookRepository.findAll()) {
            books.add(book);
        }
        return books;
	}

    @Transactional
	public Book getOne(Integer id) {
		return bookRepository.findOne(id);
	}

    @Transactional
	public void delete(int id) {
        bookRepository.delete(id);
	}

    @Transactional
	public Book add(Book book) {
		return bookRepository.save(book);
	}

    @Transactional
    public Book update(Integer id, Book book) {
        Book updatingBook = bookRepository.findOne(id);
        updatingBook.setAuthor(book.getAuthor());
        updatingBook.setName(book.getName());
        return bookRepository.save(updatingBook);
    }

    public List<Book> getInverse() {
        return bookRepository.getInverse();
    }
}
