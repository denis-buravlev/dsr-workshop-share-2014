package project.app;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import project.entity.Book;
import project.repository.BookRepository;

public class Launcher {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(MainConfig.class, args);

        BookRepository bookRepository = context.getBean(BookRepository.class);
        bookRepository.save(new Book("A Tale of Two Cities", "Charles Dickens"));
        bookRepository.save(new Book("The Lord of the Rings", "J. R. R. Tolkien"));
        bookRepository.save(new Book("Le Petit Prince", "Antoine de Saint-Exupéry"));
        bookRepository.save(new Book("Harry Potter and the Philosopher's Stone", "J. K. Rowling"));
        bookRepository.save(new Book("And Then There Were None", "Agatha Christie"));
        bookRepository.save(new Book("The Hobbit", "J. R. R. Tolkien"));
    }
}
